# Amazon Rekognition Connector

Documentation: https://docs.aws.amazon.com/rekognition

Specification: https://github.com/APIs-guru/openapi-directory/blob/main/APIs/amazonaws.com/rekognition/2016-06-27/openapi.yaml

## Prerequisites

+ Generate an AWS Access Key ID and AWS Secret Key from the AWS Admin Console

## Supported Operations
All endpoints are operational.
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

